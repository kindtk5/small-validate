const defaultMessage = `[name] is not a valid email`;

module.exports = (data, option) => {
    let { isEmail } = option;
    let len = isEmail.length;
    let errorList = [];
    let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    for (let i = 0; i < len; i += 1) {
        let { name, alias = name, msg = defaultMessage } = isEmail[i];
        let dataCheck = data[name];
        if (dataCheck === undefined || dataCheck === null) {
            continue;
        }
        if (!reg.test(String(dataCheck).toLowerCase())) {
            errorList.push(msg.replace(/\[name\]/, alias));
        }
    }
    return errorList;
}