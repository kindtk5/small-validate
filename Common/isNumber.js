const defaultMessage = `[name] is not a valid Number`;

module.exports = (data, option) => {
    let { isNumber } = option;
    let len = isNumber.length;
    let errorList = [];
    let reg = /^\d+$/;
    for (let i = 0; i < len; i += 1) {
        let { name, alias = name, msg = defaultMessage } = isNumber[i];
        let dataCheck = data[name];
        if (dataCheck === undefined || dataCheck === null) {
            return;
        }
        if (!reg.test(String(dataCheck))) {
            errorList.push(msg.replace(/\[name\]/, alias));
        }
    }
    return errorList;
}