const defaultMessage = `[name] can't be empty`;
module.exports = (data, option) => {
    let { isEmpty } = option;
    let len = isEmpty.length;
    let errorList = [];
    for (let i = 0; i < len; i += 1) {
        let { name, alias = name, msg = defaultMessage } = isEmpty[i];
        let dataCheck = data[name];
        if (dataCheck === undefined || dataCheck === null) {
            continue;
        }
        if (dataCheck.length === 0) {
            errorList.push(msg.replace(/\[name\]/, alias));
        }
    }
    return errorList;
}