const defaultMessage = `[name] is required`;

module.exports = (data, option) => {
    let { required } = option;
    let len = required.length;
    let errorList = [];
    for (let i = 0; i < len; i += 1) {
        let { name, alias = name, msg = defaultMessage } = required[i];
        let dataCheck = data[name];
        if (dataCheck === undefined || dataCheck === null || dataCheck.length === 0) {
            errorList.push(msg.replace(/\[name\]/, alias));
        }
    }
    return errorList;
}