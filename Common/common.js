const required = require('./required')
const isEmpty = require('./isEmpty')
const isNumber = require('./isNumber')
const isEmail = require('./isEmail')

module.exports = {
    required,
    isEmpty,
    isNumber,
    isEmail
}