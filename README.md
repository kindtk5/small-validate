# small-validate

A library object validators.

# Installation and Usage
Install the library with command `npm i small-validate` or `npm install small-validate`

```javascript
var { Validate } = require('small-validate');

let dataValidate = {
    userName: 'toanthien',
    passWord: '',
}

let option = {
    required: [
        { name: "userName", msg: "[name] is required", alias: "user name" },
        { name: "passWord", msg: "you must be input [name] to verify", alias: "pass word" }
    ],
    isEmpty: [
        {name: "passWord", msg: "[name] can't empty", alias: "pass word"},
        {name: "phone", msg: "[name] can't empty", alias: "phone number"}
    ],
    isNumber: [
        {name: "userName", msg: "[name] must a Number", alias: "us"},
    ]
}

console.log(Validate(dataValidate, option))

  /* return
     {
        msg: [ 'you must be input pass word to verify',
                'pass word can\'t empty',
                'us must a Number' ],
        isValid: false
    }
   */

```


### Attribute

Attribute                               | Description
--------------------------------------- | ---------------------------------------------
**name**                                | [require] object key need to check.
**msg**                                 | [option]  message return when not valid, message default is used if not exists.
**alias**                               | [option]  Alias name is replaced in param [name], if not attribute name is used.





### Operators

Validator                               | Description
--------------------------------------- | ---------------------------------------------
**required**                            | check if a string is undefined.
**isEmail**                             | check if the string is an email.
**isEmpty**                             | check if the string has a length of zero
**isNumber**                            | check if the string is contain number only.