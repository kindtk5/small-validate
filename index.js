const ListFunc = require('./Common/common')

const CleanEmpty = (srcObj) => {
    let obj = Object.assign({}, srcObj);//clone Obj
    for (let propName in obj) {
        if (obj[propName] === undefined || obj[propName] === null || obj[propName].length === 0) {
            delete obj[propName];
        }
    }
    return obj;
}

const Validate = (data, option) => {
    let keyFuncs = Object.keys(ListFunc);
    let len = keyFuncs.length;
    let msg = [];
    for (let i = 0; i < len; i += 1) {
        let key = keyFuncs[i];
        if (!option[key]) {
            continue;
        }
        let func = ListFunc[key];
        let msgCheck = func(data, option);
        msg = [...msg, ...msgCheck];
    }
    return {
        msg,
        // valid if not have message
        isValid: msg.length == 0
    }
}

module.exports = {
    Validate,
    CleanEmpty
}