const { Validate, CleanEmpty } = require('./index')

let dataTest = {
    userName: '',
    passWord: '',
}

let option = {
    required: [
        { name: "userName", msg: "[name] is required", alias: "user name" },
        { name: "passWord", msg: "you must be input [name] to verify", alias: "pass word" }
    ],
    isEmpty: [
        { name: "passWord", msg: "[name] can't empty", alias: "pass word" },
        { name: "phone", msg: "[name] can't empty", alias: "phone number" }
    ],
    isNumber: [
        { name: "userName", msg: "[name] must a Number", alias: "us" },
        { name: "passWord", msg: "[name] must a Number", alias: "pass word" },
    ]
}

console.log(Validate(dataTest, option))

console.log(CleanEmpty(dataTest))

